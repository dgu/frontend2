from datetime import datetime
from html import escape

from flask import Flask, render_template, url_for, request
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    completed = db.Column(db.Integer, default=0)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Task %r>' % self.id



@app.route('/',  methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@app.route('/menu', methods = ['GET', 'POST'])
def m():
    return render_template('menu.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return do_the_login()
    else:
        return show__the_login_form()

@app.route('/room/<string:room_name>')
def show_post(room_name):
    # show the post with the given id, the id is an integer
    return 'Room name: %s' % room_name

@app.route('/advsearch')
def advsearch():
    return render_template('advsearch.html')